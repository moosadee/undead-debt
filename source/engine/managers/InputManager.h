#ifndef _INPUT_MANAGER_HPP
#define _INPUT_MANAGER_HPP

#include <SFML/Window.hpp>

#include "../Application.h"

#include <memory>

class InputManager
{
 public:

  InputManager( sf::Window & window )
  {
    m_window = window;
  }
  
  bool PollEvent( sf::Event & event )
  {
    return m_window.pollEvent( event );
  }
  
 protected:
  sf::Window & m_window;
};

#endif
