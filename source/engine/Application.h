#ifndef _APPLICATION_HPP
#define _APPLICATION_HPP

#include <SFML/Window.hpp>

#include <string>
#include <memory>

class Application
{
 public:

  Application()
    {
      Setup();
    }
  
  Application( const std::string& title = "Chalo Game", int width = 1280, int height = 720, bool fullscreen = false )
    {
      Setup( title, width, height, fullscreen );
    }
  
  ~Application()
    {
      Cleanup();
    }
  
  void Setup( const std::string& title = "Chalo Game", int width = 1280, int height = 720, bool fullscreen = false )
  {
    m_isDone = false;
    m_window.create( sf::VideoMode( width, height ), title );
  }
  
  void Cleanup()
  {
  }
  
  bool IsDone()
  {
    return m_isDone;
  }
  
  void IsDone( bool value )
  {
    m_isDone = value;
  }
  
  sf::Window& GetWindow()
    {
      return m_window;
    }
  
  void BeginDraw()
  {
    m_window.clear( sf::Color::Green );
  }
  
  void EndDraw()
  {
    m_window.display();
  }
  
 protected:
  sf::RenderWindow m_window;
  sf::Vector2u m_screenDimensions;
  std::string m_windowTitle;
  bool m_isDone;
};

#endif 
