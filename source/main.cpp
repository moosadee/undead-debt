#include <SFML/Graphics.hpp>

#include "engine/Application.h"
#include "engine/managers/InputManager.h"

int main()
{
  Application app( "Undead Debt - Moosader", 1280, 720 );
  InputManager inputManager( app.GetWindow() );
  
  while ( !app.IsDone() )
  {
      sf::Event event;
      while ( inputManager.PollEvent( event ))
      {
          if (event.type == sf::Event::Closed)
	    {
	      app.IsDone( true );
	    }
      }

      app.BeginDraw();
      app.EndDraw();
  }

  return 0;
}
