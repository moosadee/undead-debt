# To Do, 2019-08-11

## Feature List

| Feature            | Notes                                                             | Prerequisite                                              | Size |
| ---------          | ---------                                                         | ----                                                      | ---- |
| Game screen        | In-game play and sub menus used within                            |                                                           |      |
| Menu system        | Menu markup system, Image, Button, Label, Textbox, Slider widgets |                                                           |      |
| Title screen       |                                                                   | Menu system                                               |      |
| Load screen        |                                                                   | Menu system, save system                                  |      |
| Config screen      |                                                                   | Menu system, config manager, save system, control mapper, |      |
| Character creator  |                                                                   | Menu system, lots of art, save system                     |      |
| In-game HUD        |                                                                   |                                                           |      |
| In-game town menus |                                                                   |                                                           |      |
| Screen manager     | Store last screen in a stack, be able to scroll through           |                                                           |      |
| Inventory system   | Healing items, weapons, vehicles                                  |                                                           |      |
| Loan system        |                                                                   |                                                           |      |
| Job system         | Getting new jobs, start/end points, filling orders, getting paid  |                                                           |      |
| Enemy system       | Enemy movement, tracking, attacking, grouping?                    |                                                           |      |
| Map loading        | Tiled support, collision detection                                |                                                           |      |
| Weapon system      | Aiming, shooting, ammo                                            |                                                           |      |
| Health system      |                                                                   |                                                           |      |
| Vehicle system     | On foot, horse, car (then can unlock Vroomr)                      |                                                           |      |
|                    |                                                                   |                                                           |      |

## Iterations

### Iteration 0: Initial setup / 5 minutes of fun

* Moving around the map, basic character sprite
* Zombies to avoid / fight
* Zombie sound effect subtitling and directional audio?
* HP / Basic HUD

### Iteration 1: 

* Map loading
* Controller mapping
* City menus

### Iteration 2: 20 minutes of fun

* Job system
* Shop system
* Money system
* Weapon upgrades
* Config save

### Iteration 3: More systems

* Loan system & payment system
* Savegame system
* Time system
* Vehicle system

### Iteration 4: Menus

* Menu system
* Screen manager
* Multiple screens
* Dyslexic font support

### Iteration 5: Personalization

* Character creator
* Start cutscene or tutorials
* End state

