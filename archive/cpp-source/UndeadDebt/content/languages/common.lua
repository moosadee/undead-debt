language = {
	suggested_font = "NotoSansCJKjp-Bold.ttf",

	game_title = "Fin 'n' Kit",
	game_creator = "Moosader LLC",

	language_english = "I know English",
	language_esperanto = "Mi scias Esperanton",
	language_ido = "Me savas Ido",
	language_tokipona = "toki pona",
	language_laadan = "Láadan",
	language_spanish = "I know Español",
	language_portuguese = "Português",
	language_finnish = "Suomi",
	language_french = "Français",
	language_swedish = "Svenska",
	language_italian = "Italiano",
	language_catalan = "Català",
	language_russian = "Русский",
	language_japanese = "日本語",
	language_korean = "한국어",
	language_vietnamese = "Tiếng Việt",
	language_chineseS = "简体中文",
	language_chineseT = "正體中文"
	language_interlingua = "Io sape Interlingua"
}
