#include "Player.hpp"

#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/managers/FontManager.hpp"

#include <cmath>

void Player::Setup( int startX, int startY )
{
    int w, h;
    kuko::ImageManager::AddTexture( "player", "content/graphics/player.png" );
    kuko::ImageManager::GetTextureDimensions( "player", &w, &h );

    kuko::BaseEntity::Setup( "player", kuko::ImageManager::GetTexture( "player" ), kuko::FloatRect( startX, startY, w, h ) );

    m_speed = 1000;
//    m_speed = 300;
    m_ammoCount = 20;
    m_remainingDebt = 30000;

    m_lblAmmoCount.Setup( "m_lblRemainingDebt", StringUtil::IntToString( m_ammoCount ),
        kuko::FloatRect( kuko::Application::GetDefaultWidth() - 80, 90, 100, 20 ), false, { 0xff, 0xff, 0xff, 0xff },
        kuko::FontManager::GetFont( "main" ) );

    kuko::ImageManager::AddTexture( "crosshairs", "content/graphics/crosshairs.png" );
    m_crosshairs.SetTexture( kuko::ImageManager::GetTexture( "crosshairs" ) );
    m_crosshairs.SetPosition( kuko::FloatRect( 0, 0, 48, 48 ) );
}

void Player::Update()
{
    kuko::BaseEntity::Update();

    for ( unsigned int i = 0; i < m_bullets.size(); i++ )
    {
        m_bullets[i].Update();
    }
}

void Player::RegisterScreenOffset( kuko::IntRect* ptrOffset )
{
    m_ptrScreenOffset = ptrOffset;
}

void Player::Draw()
{
    for ( unsigned int i = 0; i < m_bullets.size(); i++ )
    {
        m_bullets[i].Draw( *m_ptrScreenOffset );
    }

    if ( m_ptrScreenOffset == NULL )
    {
        m_sprite.Draw();
    }
    else
    {
        m_sprite.DrawWithOffset( *m_ptrScreenOffset );
    }
    m_lblAmmoCount.Draw();
    m_crosshairs.Draw();
}

void Player::HandleMovement( std::map<kuko::CommandButton, kuko::TriggerInfo>& input )
{
    if ( input[ kuko::MOVE_UP ].down )
    {
        m_position.y -= m_speed * kuko::Application::GetTimeStep();
    }
    else if ( input[ kuko::MOVE_DOWN ].down )
    {
        m_position.y += m_speed * kuko::Application::GetTimeStep();
    }

    if ( input[ kuko::MOVE_LEFT ].down )
    {
        m_position.x -= m_speed * kuko::Application::GetTimeStep();
    }
    else if ( input[ kuko::MOVE_RIGHT ].down )
    {
        m_position.x += m_speed * kuko::Application::GetTimeStep();
    }

    if ( input[ kuko::MOUSE_DOWN ].down )
    {
        CreateBullet();
    }


    UpdateSprite();
}

void Player::UpdateCrosshairs( int x, int y )
{
    m_crosshairs.SetPosition( x, y );
}

void Player::CreateBullet()
{
    if ( m_ptrScreenOffset == NULL )
    {
        return;
    }

    // Get angle between player and the mouse.
    int playerXAdjust = m_position.x + m_ptrScreenOffset->x + ( m_position.w / 2 );
    int playerYAdjust = m_position.y + m_ptrScreenOffset->y + ( m_position.h / 2 );

    int crossXAdjust = m_crosshairs.GetPosition().x + ( m_crosshairs.GetPosition().w / 2 );
    int crossYAdjust = m_crosshairs.GetPosition().y + ( m_crosshairs.GetPosition().h / 2 );

    int xDiff = crossXAdjust - playerXAdjust;
    int yDiff = crossYAdjust - playerYAdjust;

    float angle = atan2( yDiff, xDiff );

//    Logger::Out(    " Player screen x:  " + StringUtil::IntToString( playerXAdjust ) +
//                    " Player screen y:  " + StringUtil::IntToString( playerYAdjust ), "Player::CreateBullet" );
//
//    Logger::Out(    " Crosshair x:      " + StringUtil::IntToString( m_crosshairs.GetPosition().x ) +
//                    " Crosshair y:      " + StringUtil::IntToString( m_crosshairs.GetPosition().y ), "Player::CreateBullet" );
//
//    Logger::Out(    " X:      " + StringUtil::IntToString( xDiff ) +
//                    " Y:      " + StringUtil::IntToString( yDiff ), "Player::CreateBullet" );
//
//    Logger::Out(    " ArcTan:           " + StringUtil::FloatToString( angle * 180 / 3.14159 ), "Player::CreateBullet" );


    Bullet bullet;
    bullet.Setup( m_position.x + ( m_position.w / 2 ), m_position.y + ( m_position.h / 2 ), angle );
    m_bullets.push_back( bullet );

    Logger::Out( StringUtil::IntToString( m_bullets.size() ) + " player bullets", "Player::CreateBullet" );
}
