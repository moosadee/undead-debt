#include "Map.hpp"

#include <cstdlib>

#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/base/Application.hpp"

// Screen resolution is 1280x720, base tiles are 64x64
// so one screen is 20x11.25

void Map::Generate()
{
    LoadImages();

    m_worldWidth = 12 * 10;
    m_worldHeight = 12 * 10;
    m_tileWH = 64;

    Logger::Out( "Map dimensions: " + StringUtil::IntToString( m_worldWidth * m_tileWH ) + " x " + StringUtil::IntToString( m_worldHeight * m_tileWH ), "Map::Generate" );

    // Build base floor
    for ( int y = 0; y < m_worldHeight; y++ )
    {
        for ( int x = 0; x < m_worldWidth; x++ )
        {
            kuko::Sprite tile;
            tile.SetTexture( kuko::ImageManager::GetTexture( "tile_grass" ) );
            tile.SetPosition( kuko::FloatRect( x * m_tileWH, y * m_tileWH, m_tileWH, m_tileWH ) );
            m_baseTiles.push_back( tile );
        }
    }

    // Add accents
    int longGrassCount = rand() % 1000 + 100;
    for ( int i = 0; i < longGrassCount; i++ )
    {
        int randX = rand() % ( m_worldWidth * m_tileWH );
        int randY = rand() % ( m_worldHeight * m_tileWH );

        int w, h;
        kuko::ImageManager::GetTextureDimensions( "tile_longgrass", &w, &h );

        kuko::Sprite tile;
        tile.SetTexture( kuko::ImageManager::GetTexture( "tile_longgrass" ) );
        tile.SetPosition( kuko::FloatRect( randX, randY, w, h ) );
        m_topTiles.push_back( tile );
    }

    // Add cities:
    kuko::ImageManager::AddTexture( "kansascity", "content/graphics/town_kansascity.png" );;

    m_cities.push_back( City( "Kansas City",    "kansascity", 5100, 3800 ) );
    Logger::Debug( "KC X " + StringUtil::IntToString( m_cities[0].lblName.GetPosition().x ) );
    m_cities.push_back( City( "Topeka",         "kansascity", 2400, 5000 ) );
    m_cities.push_back( City( "Columbia",       "kansascity", 6000, 5000 ) );
    m_cities.push_back( City( "Omaha",          "kansascity", 2560, 500 ) );
//    m_cities.push_back( City( "Lincoln",        "kansascity", 500, 2000 ) );
    m_cities.push_back( City( "Lincoln",        "kansascity", 2000, 2000 ) );
    m_cities.push_back( City( "Wichita",        "kansascity", 400, 7000 ) );
    m_cities.push_back( City( "Springfield",    "kansascity", 5500, 7000 ) );

    Logger::Debug( "KC X " + StringUtil::IntToString( m_cities[0].lblName.GetPosition().x ) );
}

void Map::LoadImages()
{
    kuko::ImageManager::AddTexture( "tile_grass", "content/graphics/terrain/tile_grass.png" );
    kuko::ImageManager::AddTexture( "tile_longgrass", "content/graphics/terrain/decor_longgrass.png" );
}

void Map::Draw( kuko::IntRect& screenOffset )
{
    for ( unsigned int i = 0; i < m_baseTiles.size(); i++ )
    {
        m_baseTiles[i].DrawWithOffset( screenOffset );
    }
    for ( unsigned int i = 0; i < m_topTiles.size(); i++ )
    {
        m_topTiles[i].DrawWithOffset( screenOffset );
    }
//    Logger::Debug( "Draw cities" );
    for ( unsigned int i = 0; i < m_cities.size(); i++ )
    {
        m_cities[i].Draw( screenOffset );
    }
}

void Map::DrawMinimap( const Player& player )
{
    int width = 200, height = 200;
    int x = kuko::Application::GetDefaultWidth() - width - 10;
    int y = kuko::Application::GetDefaultHeight() - height - 10;

    float mapRatio = float( width ) / float( m_worldWidth * m_tileWH );

    kuko::ImageManager::DrawRectangle( kuko::FloatRect( x, y, width, height ), 255, 255, 255, 1, false );

    // Draw cities
    for ( unsigned int i = 0; i < m_cities.size(); i++ )
    {
        float mapX = m_cities[i].sprite.GetPosition().x * mapRatio;
        float mapY = m_cities[i].sprite.GetPosition().y * mapRatio;

        kuko::ImageManager::DrawRectangle( kuko::FloatRect( mapX-1 + x, mapY-1 + y, 4, 4 ), 255, 222, 0, 1, true );
    }

    // Draw player
    float px = player.GetPosition().x * mapRatio;
    float py = player.GetPosition().y * mapRatio;
    kuko::ImageManager::DrawRectangle( kuko::FloatRect( px-1 + x, py-1 + y, 4, 4 ), 255, 255, 255, 1, true );
}
