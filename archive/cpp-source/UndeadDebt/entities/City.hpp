#ifndef CITY_HPP
#define CITY_HPP

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/widgets/UILabel.hpp"
#include "../Kuko/managers/FontManager.hpp"

class City
{
    public:
    City();
    City( const std::string& name, const std::string& textureName, int x, int y );

    kuko::Sprite sprite;
    std::string name;
    kuko::Sprite lblName;

    void Setup( const std::string& name, const std::string& textureName, int x, int y );
    void Draw( kuko::IntRect& screenOffset );
};

#endif
