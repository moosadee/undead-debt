#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/entities/BaseEntity.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/widgets/UILabel.hpp"

#include "Bullet.hpp"

#include <vector>

class Player : public kuko::BaseEntity
{
    public:
    void Setup( int startX, int startY );
    void Draw();
    void Update();

    void HandleMovement( std::map<kuko::CommandButton, kuko::TriggerInfo>& input );

    int GetRemainingDebt() { return m_remainingDebt; }
    void ReduceDebt( int amt ) { m_remainingDebt -= amt; }

    void RegisterScreenOffset( kuko::IntRect* ptrOffset );

    void UpdateCrosshairs( int x, int y );

    private:
    void CreateBullet();

    float m_speed;
    int m_ammoCount;
    kuko::UILabel m_lblAmmoCount;
    int m_remainingDebt;
    kuko::Sprite m_crosshairs;
    std::vector<Bullet> m_bullets;

    kuko::IntRect* m_ptrScreenOffset;
};

#endif
