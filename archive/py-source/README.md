# Ezha-Template-Project

A starter project file to go with the Ezha engine

Clone the [ezha engine](https://github.com/Moosader/ezha)
so that it is a subfolder of your main game source directory.

## Starter files

* assets
    * fonts - put fonts here, has a starter font
    * graphics - put graphics here
        * ui - some starter images
    * language - contains csv files for languages
    * menus - contains menu markup files
* states - put game states here, includes starter title, help, options states.
* ezha - clone the [ezha](https://github.com/Moosader/ezha) repository in the project folder and you're good to go
* config.csv - contains some settings options

## Starter project support

The following features are implemented as part of this template project:

* Title screen, with working paths to the game state / options state / help state, and a working exit button.
* Help screen, with placeholder text.
* Options screen, with working sound and volume -/+ buttons.
* Small game state
    * Movable character object
    * Loadable map built with Tiled
    * Tile-based collision detection

## Ezha engine support

* Loading Flare maps exported from Tiled, including collision detection and "under-player" and "over-player" tile support.
* Multiple language files, stored in .csv documents
* Menu building with Ezha's plaintext markup language
* Basic object classes for GameObjects, Tiles, Maps, and Characters.
* Basic UI widgets: Button, Image, Label
* Support for a configuration .csv file

## Screenshots

![Title screen](_screenshots/titlestate.png)

![Options screen](_screenshots/optionsstate.png)

![Game screen](_screenshots/gamestate.png)
