import pygame, sys
from pygame.locals import *

import random

from objects import Enemy, SadPlayer, Bullet
from managers import EffectManager
from ezha       import Utilities

class CharacterManager:
    enemies = []
    bullets = []
    player = None
    
    @classmethod
    def AddPlayer( pyClass, image ):
        pyClass.player = SadPlayer()
        #self.player.Setup( self.images["player"], 4000, 1980, 32, 48 )
        pyClass.player.Setup( image, 500, 688, 66, 86 )
        pyClass.player.SetupAnimation( 4, 0.1, 0.4 )
        pyClass.player.SetCollisionRegion( 0, 32, 32, 12 )
        pyClass.player.SetSpeed( 10, 20 )
        pyClass.player.SetInputs( {    # Defaults to the arrow keys
            "up" : K_w,
            "down" : K_s,
            "left" : K_a,
            "right" : K_d,
            "run" : K_LSHIFT
            }, True     # Allow diagonal movement
            )
        pyClass.player.SetAnimationMappings( { "idle-south" : 0, "idle-north" : 1, "idle-west" : 2, "idle-east" : 3,
                                            "walk-south" : 4, "walk-north" : 5, "walk-west" : 6, "walk-east" : 7,
                                            "run-south"  : 8, "run-north"  : 9, "run-west" : 10, "run-east" : 11
                                            } )
        pyClass.player.ChangeAnimationAction( "idle-south" )
        
    @classmethod
    def GetPlayer( pyClass ):
        return pyClass.player
    
    @classmethod
    def AddEnemy( pyClass, options ):
        newEnemy = Enemy()
        newEnemy.Setup( options["image"], options["x"], options["y"], options["width"], options["height"] )
        newEnemy.SetupAnimation( 4, 0.1, 0.4 )
        newEnemy.SetCollisionRegion( 0, 32, 32, 12 )
        newEnemy.SetSpeed( 1, 5 )
        
        newEnemy.SetAnimationMappings( { "idle-south" : 0, "idle-north" : 1, "idle-west" : 2, "idle-east" : 3,
                                            "walk-south" : 4, "walk-north" : 5, "walk-west" : 6, "walk-east" : 7
                                            } )
        newEnemy.ChangeAnimationAction( "idle-south" )
        
        pyClass.enemies.append( newEnemy )
        
    @classmethod
    def GenerateEnemies( pyClass, amount, map, image ):
        mapWidth = map.GetPixelWidth()
        mapHeight = map.GetPixelHeight()
        
        imageWidth, imageHeight = image.get_rect().size
        
        for i in range( amount ):
            randX = random.randint( 0, mapWidth )
            randY = random.randint( 0, mapHeight )
            pyClass.AddEnemy( { "image" : image, "x" : randX, "y" : randY, "width" : 66, "height" : 86 } )
            
    @classmethod
    def PlayerShoot( pyClass, camera, mouseX, mouseY, bulletImage ):
        if ( pyClass.player.currentGun is None or pyClass.player.currentGun.CanShoot() == False ):
            return
            
        pyClass.player.currentGun.Shoot()
        playerScreenRect = pyClass.player.GetScreenPositionRect( camera )
        playerMapRect = pyClass.player.GetRect()
        newBullet = Bullet()
        newBullet.Setup( {
            "playerx" : playerScreenRect.x + playerScreenRect.width/2,
            "playery" : playerScreenRect.y + playerScreenRect.height/2,
            "mousex" : mouseX,
            "mousey" : mouseY,
            "startx" : playerMapRect.x + playerScreenRect.width/2,
            "starty" : playerMapRect.y + playerScreenRect.height/2,
            "speed" : 10,
            "image" : bulletImage
        } )
        pyClass.bullets.append( newBullet )
            
    @classmethod
    def Update( pyClass, keyPress, levelMap, camera, timeCounter, monthLength, effectManager ):
        # Update Player
        movedX, movedY = pyClass.player.Update( keyPress, levelMap, timeCounter, monthLength, effectManager, camera ) 
        camera.CenterOn( pyClass.player.GetRect() )
        
        # Update Enemy
        for enemy in pyClass.enemies:
            enemy.Update( pyClass.player )
            
            if ( Utilities.GetDistance( pyClass.player.GetRect(), enemy.GetRect() ) < 10 ):
                gotHurt = pyClass.player.GetHurt( enemy.GetDamage() )
                if ( gotHurt == True ):
                    effectManager.PutFloatyTextAtPlayer( "-" + str( enemy.GetDamage() ), pygame.Color( 255, 255, 255 ), camera, pyClass.player )
        
        # Update Bullets
        deleteBullets = []
        for key in range( 0, len( pyClass.bullets ) ):
            pyClass.bullets[key].Update()
            
            if ( pyClass.bullets[key].IsActive() == False ):
                deleteBullets.append( key )
                
        for delkey in deleteBullets:
            pyClass.bullets.pop( delkey )
        
    @classmethod
    def Draw( pyClass, window, camera ):
        for enemy in pyClass.enemies:
            enemy.Draw( window, camera )
            
        pyClass.player.Draw( window, camera )
        
        for bullet in pyClass.bullets:
            bullet.Draw( window, camera )
