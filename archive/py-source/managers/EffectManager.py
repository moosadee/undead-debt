import pygame, sys
from pygame.locals import *

from ezha       import Effects
from ezha import FontManager

class EffectManager:
    effects = Effects()    # Used for text effects (floating numbers)
   
    @classmethod
    def Update( pyClass ):
        pyClass.effects.Update()
        
    @classmethod
    def Draw( pyClass, window ):
        pyClass.effects.Draw( window )
   
    @classmethod
    def PutFloatyTextAtPlayer( pyClass, text, color, camera, player ):
        playerRect = player.GetScreenPositionRect( camera )
        pyClass.effects.AddLabelEffect( { "effect" : "floatup", "label" : {
            "label_font" : FontManager.Get( "hud_sub" ),
            "label_text" : text,
            "label_position" : ( playerRect.x, playerRect.y ),
            "label_color" :color,
            "label_secondary_color" : pygame.Color( 0, 0, 0 ), 
            "label_style" : "outline"
            } } )
