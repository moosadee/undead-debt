#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from states import TitleState
from states import GameState
from states import HelpState
from states import OptionsState
from states import NewGameState
from states import LanguageSelectState
from ezha import ConfigManager
from ezha import FontManager
from ezha import LanguageManager
from ezha import SoundManager
from ezha import MenuManager
from ezha import Utilities
from ezha import Logger

class Program( object ):
    
    def __init__( self ):
        self.fpsClock = None
        self.windowSurface = None
        self.currentState = None
        self.screenWidth = 1280
        self.screenHeight = 720
        self.isDone = False
        self.fps = 30
        self.clearColor = pygame.Color( 151, 255, 140 )
        self.states = {}
    
    def Init( self ):
        Logger.Init( False )
        Logger.Write( "Logger starts", "Program::Init" )
        ConfigManager.Load()

        self.screenWidth = int( ConfigManager.options[ "screenwidth" ] )
        self.screenHeight = int( ConfigManager.options[ "screenheight" ] )
        
        pygame.init()
        self.fpsClock = pygame.time.Clock()
        Utilities.SetFPSClock( self.fpsClock )
        self.windowSurface = pygame.display.set_mode( ( self.screenWidth, self.screenHeight ) )

        
        language = ConfigManager.options[ "language" ].lower()
        
        SoundManager.Add( "confirm", "assets/audio/ui-confirm.wav", False )
        SoundManager.Add( "cancel", "assets/audio/ui-cancel.wav", False )
        SoundManager.SetMusicVolume( float( ConfigManager.options["music volume"] ) / 100 )
        SoundManager.SetSoundVolume( float( ConfigManager.options["sound volume"] ) / 100 )
        MenuManager.SetButtonAudio( "confirm", "cancel" )
        
        LanguageManager.Setup( { "helperLanguage" : language, "targetLanguage" : language } )
        
        pygame.display.set_caption( "Undead Debt" )
        FontManager.Add( "main", "assets/fonts/Blogger Sans-Bold.otf", 60 )
        FontManager.Add( "main_big", "assets/fonts/Blogger Sans-Bold.otf", 280 )
        FontManager.Add( "hud", "assets/fonts/Blogger Sans-Bold.otf", 30 )
        FontManager.Add( "hud_sub", "assets/fonts/Blogger Sans-Bold.otf", 25 )
        FontManager.Add( "hud_sub2", "assets/fonts/Blogger Sans-Bold.otf", 18 )

        self.states[ "titlestate" ] = TitleState()
        self.states[ "gamestate" ] = GameState()
        self.states[ "optionsstate" ] = OptionsState()
        self.states[ "helpstate" ] = HelpState()
        self.states[ "newgamestate" ] = NewGameState()
        self.states[ "languageselectstate" ] = LanguageSelectState()

        self.SwitchState( "gamestate" )
        
    def Cleanup( self ):
        Logger.Free()
        
    def Run( self ):
        while ( self.isDone is False ):
            self.Update()
            self.Draw()

            gotoState = self.currentState.GotoState()
            if ( gotoState != "" ):
                self.SwitchState( gotoState )
        
        self.Cleanup()
    
    def Update( self ):
        self.currentState.Update()
        
    def Draw( self ):
        self.windowSurface.fill( self.clearColor )
        
        self.currentState.Draw( self.windowSurface )
        
        pygame.display.update()
        self.fpsClock.tick( self.fps )

    def SwitchState( self, name ):
        if ( name == "exit" ):
            print( "Goodbye!" )
            pygame.quit()
            sys.exit()
        
        if ( self.currentState != None ):
            self.currentState.Clear()
            
        self.currentState = self.states[ name ]
        self.currentState.Setup( self.screenWidth, self.screenHeight )
