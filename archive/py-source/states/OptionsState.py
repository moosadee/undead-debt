#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha import BaseState
from ezha import FontManager
from ezha import LanguageManager
from ezha import ConfigManager
from ezha import MenuManager
from ezha import SoundManager
from ezha import Label

class OptionsState( BaseState ):
    def __init__( self ):
        self.stateName = "optionsstate"
        self.labelSoundVolume = None
        self.labelMusicVolume = None
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""

        MenuManager.Load( "assets/menus/optionsscreen.menu" )

        self.labelSoundVolume = Label()
        self.labelSoundVolume.Setup( {
            "label_font"              : FontManager.Get( "main" ),
            "label_text"              : str( int( SoundManager.GetSoundVolume() * 100 ) ) + "%",
            "label_position"          : ( 200, 200 ),
            "label_color"             : pygame.Color( 255, 255, 255 ),
            "label_style"             : "outline",
            "label_secondary_color"   : pygame.Color( 0, 0, 0 )
            } )

        self.labelMusicVolume = Label()
        self.labelMusicVolume.Setup( {
            "label_font"              : FontManager.Get( "main" ),
            "label_text"              : str( int( SoundManager.GetMusicVolume() * 100 ) ) + "%",
            "label_position"          : ( 200, 400 ),
            "label_color"             : pygame.Color( 255, 255, 255 ),
            "label_style"             : "outline",
            "label_secondary_color"   : pygame.Color( 0, 0, 0 )
            } )
        
        
    def Update( self ):        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if event.type == MOUSEBUTTONUP:
                mouseX, mouseY = event.pos    

                clickAction = MenuManager.ButtonAction( mouseX, mouseY )
                
                if ( clickAction == "music_down" ):
                    SoundManager.SetMusicVolume( SoundManager.GetMusicVolume() - 0.10 )
                    self.labelMusicVolume.ChangeText( str( int( SoundManager.GetMusicVolume() * 100 ) ) + "%" )
                    
                elif ( clickAction == "music_up" ):
                    SoundManager.SetMusicVolume( SoundManager.GetMusicVolume() + 0.10 )
                    self.labelMusicVolume.ChangeText( str( int( SoundManager.GetMusicVolume() * 100 ) ) + "%" )
                    
                elif ( clickAction == "sound_down" ):
                    SoundManager.SetSoundVolume( SoundManager.GetSoundVolume() - 0.10 )
                    self.labelSoundVolume.ChangeText( str( int( SoundManager.GetSoundVolume() * 100 ) ) + "%" )
                    
                elif ( clickAction == "sound_up" ):
                    SoundManager.SetSoundVolume( SoundManager.GetSoundVolume() + 0.10 )
                    self.labelSoundVolume.ChangeText( str( int( SoundManager.GetSoundVolume() * 100 ) ) + "%" )
                    
                elif ( clickAction != "" ):
                    ConfigManager.ChangeValue( "sound volume", str( int( SoundManager.GetSoundVolume() * 100 ) ) )
                    ConfigManager.ChangeValue( "music volume", str( int( SoundManager.GetMusicVolume() * 100 ) ) )
                    ConfigManager.Save()
                    self.gotoState = clickAction  
    
    
    def Draw( self, windowSurface ):
        MenuManager.Draw( windowSurface )

        self.labelSoundVolume.Draw( windowSurface )
        self.labelMusicVolume.Draw( windowSurface )
        

    def Clear( self ):
        MenuManager.Clear()
        
