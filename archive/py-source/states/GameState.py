#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys, math
from pygame.locals import *

import random

from ezha       import BaseState, Logger
from ezha       import FontManager, LanguageManager, ConfigManager, MenuManager
from ezha       import Utilities
from ezha       import Label, Image, Menu
from ezha       import Map, Camera, Effects
from objects    import SadPlayer, Bullet
from managers   import MenuManager, CharacterManager, EffectManager
from inventory import Loan, Job, Gun, Money, Health, Status

class GameState( BaseState ):
    def __init__( self ):
        self.stateName = "gamestate"
        self.images = {}
        self.levelMap = None
        self.camera = None
        self.mouseX = 0
        self.mouseY = 0
        self.jobPay = { "letter" : 0, "food" : 0, "vroomr" : 0 }
        self.costs = { "handgun" : 100, "ammo" : 20 }
        self.gameStart = start_ticks = pygame.time.get_ticks()
        self.timeCounter = 0
        self.monthLength = 20       # seconds per month
        self.cityLabels = []        # Temporary
        self.cityLookup = []
        self.hud = Menu()
        self.menu = Menu()
        self.restCost = 25


    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""

        self.LoadImages()

        CharacterManager.AddPlayer( self.images["player"] )
        self.playerRef = CharacterManager.GetPlayer()

        self.levelMap = Map()
        #self.levelMap.LoadJsonMap( "assets/tiledmaps/usv2.json", self.images["tileset"] )
        self.levelMap.LoadJsonMap( "assets/tiledmaps/usv3.json", self.images["tileset"] )

        CharacterManager.GenerateEnemies( 20, self.levelMap, self.images["zombie"] )

        # Hud width
        width = int(ConfigManager.options[ "screenwidth" ]) / 4
        height = int(ConfigManager.options[ "screenheight" ])
        
        self.camera = Camera()
        self.camera.Setup( screenWidth - width, int(ConfigManager.options[ "screenheight" ]) )
        #self.camera.SetRange( 0, 0, 320, 240 )
        self.camera.SetRange( 0, 0, self.levelMap.GetPixelWidth(), self.levelMap.GetPixelHeight() )
        # map is 1600 x 960
        
        MenuManager.BuildHud( self.hud, self.images, self.playerRef )

        for city in self.levelMap.GetObjects():
            newLabel = Label()
            newLabel.Setup( {
                "label_font" : FontManager.Get( "main" ),
                "label_text" : city["name"],
                "label_position" : ( city["x"], city["y"] ),
                "label_color" : pygame.Color( 255, 255, 255 ),
                "label_secondary_color" : pygame.Color( 0, 0, 0 ),
                "label_style" : "outline"
            } )
            self.cityLabels.append( newLabel )
            self.cityLookup.append( city["name"] )
    
    
    def Update( self ):
        self.HandleEvents()
        EffectManager.Update()

        self.timeCounter = ( pygame.time.get_ticks() - self.gameStart ) / 1000
        
        keys = pygame.key.get_pressed()
        CharacterManager.Update( keys, self.levelMap, self.camera, self.timeCounter, self.monthLength, EffectManager )

        if ( keys[ K_z ] ): #TEMPORARY
            EffectManager.PutFloatyTextAtPlayer( "Player: " + str( self.playerRef.GetRect().x ) + ", " + self.playerRef.GetRect().y, pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )

        self.UpdateStatusText()
        self.hud.UpdateLabelText( "fps", "FPS: " + str( int( Utilities.GetFPSClock().get_fps() ) ) )

        # A month has passed
        if ( self.timeCounter != 0 and self.timeCounter % self.monthLength == 0 ):
            amountAdded = None
            for key, loan in self.playerRef.loans.iteritems():
                amountAdded = loan.Update( self.timeCounter )
            
            if ( amountAdded is not None ):
                self.loansUpdated = True
                EffectManager.PutFloatyTextAtPlayer( "Loans+", pygame.Color( 255, 0, 0 ), self.camera, self.playerRef )
    
            self.UpdateStatusText()
            


    def HandleMouse( self,  event ):
        self.mouseX, self.mouseY = event.pos

        nearCity = self.IsNearCity( self.playerRef, self.levelMap.GetObjects() )

        if ( nearCity != "" and MenuManager.IsMenuOpen( self.menu ) == False ):
            MenuManager.BuildCityMenu( self.menu, self.images, nearCity, self.playerRef )

        menuClick = self.menu.ClickedButton( self.mouseX, self.mouseY )
        if ( menuClick != "" ):
            print( menuClick )
        
        # Menu buttons
        if ( menuClick == "leavecity" ):
            self.menu.Clear()

        elif ( menuClick == "loan" ):
            MenuManager.BuildLoanMenu( self.menu, self.images, self.playerRef )
            
        elif ( menuClick == "shop" ):
            MenuManager.BuildShopMenu( self.menu, self.images, nearCity, self.playerRef, self.costs )
            
        elif ( menuClick == "loancancel" ):
            MenuManager.BuildCityMenu( self.menu, self.images, nearCity, self.playerRef )
            
        elif ( menuClick == "job" ):            self.Menu_Jobs( nearCity )
        elif ( menuClick == "buy-ammo" ):       self.Menu_ShopBuyAmmo( nearCity )
        elif ( menuClick == "buy-weapon" ):     self.Menu_ShopBuyWeapon( nearCity )
        elif ( menuClick == "job-deliver" ):    self.Menu_JobDeliveryLetter( nearCity )
        elif ( menuClick == "job-food" ):       self.Menu_JobDeliveryFood( nearCity )
        elif ( menuClick == "job-vroomr" ):     self.Menu_JobVroomr()
        elif ( menuClick == "deliver-letter" or menuClick == "deliver-food" ): self.Menu_JobDeliveryFinish()
        elif ( menuClick == "rest" ):           self.Menu_Rest()
        elif ( menuClick == "loanpay-min" ):    self.Menu_PayMinLoan()
            
        if ( nearCity == "" ):
            self.Shoot()

    def Shoot( self ):
        if ( self.playerRef.currentGun is None ):
            EffectManager.PutFloatyTextAtPlayer( "No weapons", pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )
        elif ( self.playerRef.currentGun.GetAmmo() == 0 ):
            EffectManager.PutFloatyTextAtPlayer( "No ammo", pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )
        else:
            CharacterManager.PlayerShoot( self.camera, self.mouseX, self.mouseY, self.images["bullet"] )

    def Menu_PayMinLoan( self ):
        if ( self.playerRef.money.GetAmount() < self.playerRef.loans["studentLoan"].GetMinimumPayment() ):
            EffectManager.PutFloatyTextAtPlayer( "Can't afford!", pygame.Color( 255, 0, 0 ), self.camera, self.playerRef )
        
        else:
            self.playerRef.PayStudentLoans( self.playerRef.loans["studentLoan"].GetMinimumPayment() )
            EffectManager.PutFloatyTextAtPlayer( "-$" + self.playerRef.loans["studentLoan"].GetMinimumpaymentString(), pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )
            self.UpdateStatusText()
            #self.menu.Clear()

    def Menu_Rest( self ):
        if ( self.playerRef.GetMoney() < self.restCost ):
            EffectManager.PutFloatyTextAtPlayer( "Can't afford!", pygame.Color( 255, 0, 0 ), self.camera, self.playerRef )
        
        else:
            self.playerRef.SetHealth( 100 )
            self.playerRef.LoseMoney( self.restCost )
            EffectManager.PutFloatyTextAtPlayer( "-$" + str( self.restCost ), pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )
            self.UpdateStatusText()
            self.menu.Clear()

    def Menu_ShopBuyWeapon( self, nearCity ):
        if ( self.playerRef.money.GetAmount() < self.costs["handgun"] ):
            EffectManager.PutFloatyTextAtPlayer( "Can't afford!", pygame.Color( 255, 0, 0 ), self.camera, self.playerRef )
        else:
            self.playerRef.money.Subtract( self.costs["handgun"] )
            newGun = Gun( { "name" : "handgun", "image" : self.images["handgun"], "reloadSpeed" : 10, "bulletStyle" : "single", "ammoImage" : self.images["ammo"] } )
            self.playerRef.guns.append( newGun )
            self.playerRef.currentGun = newGun
            self.UpdateStatusText()
            EffectManager.PutFloatyTextAtPlayer( "-$" + str( self.costs["handgun"] ), pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )
            self.costs.pop( "handgun" )
            MenuManager.BuildShopMenu( self.menu, self.images, nearCity, self.playerRef, self.costs )

    def Menu_ShopBuyAmmo( self, nearCity ):
        if ( self.playerRef.money.GetAmount() < self.costs["ammo"] ):
            EffectManager.PutFloatyTextAtPlayer( "Can't afford!", pygame.Color( 255, 0, 0 ), self.camera, self.playerRef )
        elif ( self.playerRef.currentGun is None ):
            EffectManager.PutFloatyTextAtPlayer( "You don't have a gun!", pygame.Color( 255, 0, 0 ), self.camera, self.playerRef )
        else:
            self.playerRef.currentGun.AddAmmo( 20 )
            self.playerRef.money.Subtract( self.costs["ammo"] )
            EffectManager.PutFloatyTextAtPlayer( "-$" + str( self.costs["ammo"] ), pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )

    def Menu_Jobs( self, nearCity ):
        self.jobPay["letter"] = random.randint( 10, 30 )
        self.jobPay["food"] = random.randint( 20, 40 )
        self.jobPay["vroomr"] = random.randint( 70, 100 )
        MenuManager.BuildJobMenu( self.menu, self.images, nearCity, self.playerRef, { "jobPay" : self.jobPay } )

    def Menu_JobDeliveryFinish( self ):
        self.playerRef.money.Add( self.playerRef.job.GetPay() )
        EffectManager.PutFloatyTextAtPlayer( "+$" + str( self.playerRef.job.GetPay() ), pygame.Color( 255, 255, 0 ), self.camera, self.playerRef )
        self.playerRef.job = None
        self.menu.Clear()
        
    def Menu_JobVroomr( self ):
        EffectManager.PutFloatyTextAtPlayer( "Must have a car!", pygame.Color( 255, 0, 0 ), self.camera, self.playerRef )
        
    def Menu_JobDeliveryFood( self, nearCity ):
        randomCity = random.randint( 0, len( self.cityLookup ) - 1 )
        city = self.cityLookup[ randomCity ]
        
        self.playerRef.job = Job( { "image" : self.images["fastfood"], "pay" : self.jobPay["food"], "source" : nearCity, "destination" : city, "type" : "deliver-food" } )
        self.playerRef.status.Set( "Deliver food to " + city )
        self.menu.Clear()
        
        EffectManager.PutFloatyTextAtPlayer( self.playerRef.status.Get(), pygame.Color( 0, 255, 255 ), self.camera, self.playerRef )
        
    def Menu_JobDeliveryLetter( self, nearCity ):
        randomCity = random.randint( 0, len( self.cityLookup ) - 1 )
        city = self.cityLookup[ randomCity ]
        
        self.playerRef.job = Job( { "image" : self.images["statement"], "pay" : self.jobPay["letter"], "source" : nearCity, "destination" : city, "type" : "deliver-letter" } )
        self.playerRef.status.Set( "Deliver letter to " + city )
        self.menu.Clear()
        
        EffectManager.PutFloatyTextAtPlayer( self.playerRef.status.Get(), pygame.Color( 0, 255, 255 ), self.camera, self.playerRef )

    def IsNearCity( self, character, cities ):
        charCenterX = character.GetRect().x + character.GetRect().width / 2
        charCenterY = character.GetRect().y + character.GetRect().height / 2

        for city in cities:
            cityCenterX = city["x"] + city["width"] / 2
            cityCenterY = city["y"] + city["height"] / 2

            if ( Utilities.GetPointDistance( cityCenterX, cityCenterY, charCenterX, charCenterY ) < 100 ):
                return city["name"]

        return ""


    def UpdateStatusText( self ):
        self.hud.UpdateLabelText( "statusNote", self.playerRef.status.Get() )
        self.hud.UpdateLabelText( "money", self.playerRef.money.GetAmountString() )
        
        # TODO: Fix to have appropriate icon for items
        #if ( self.playerRef.job is None ):
        #    self.hud.UpdateImageTexture( "inventory1", self.images["empty"] )
        #else:
        #    self.hud.UpdateImageTexture( "inventory1", self.playerRef.job.GetImage() )
            
        #if ( self.playerRef.currentGun is None ):
        #    self.hud.UpdateImageTexture( "inventory2", self.images["empty"] )
        #    self.hud.UpdateImageTexture( "inventory3", self.images["empty"] )
        #else:
        #    self.hud.UpdateImageTexture( "inventory2", self.playerRef.currentGun.GetImage() )
        #    self.hud.UpdateImageTexture( "inventory3", self.playerRef.currentGun.GetAmmoImage() )
        
        for key, loan in self.playerRef.loans.iteritems():
            self.hud.UpdateLabelText( "principle" + loan.GetName(), loan.GetPrincipleString() )
            self.hud.UpdateLabelText( "interest" + loan.GetName(), loan.GetInterestString() )

        self.hud.UpdateLabelText( "ammoCount", "" )
        
        self.hud.UpdatePercentBarAmount( "health", self.playerRef.health.GetAmount() )
        
        
        nearCity = self.IsNearCity( self.playerRef, self.levelMap.GetObjects() )

        if ( nearCity != "" and self.hud.HasElement( "enterTown" ) == False ):
            pos = self.playerRef.GetActualPosition( self.camera )

            self.hud.AddLabel( "enterTown",
                { "label_font" : FontManager.Get( "hud_sub" ),
                "label_text" : "Enter " + nearCity,
                "label_position" : ( pos[0] - pos[2]/2, pos[1] + pos[3] ),
                "label_color" : pygame.Color( 255, 255, 255 ), "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )

            self.hud.AddImage( "enterTownIcon",
                { "image_texture" : self.images[ "mouseicon" ],
                "image_position" : ( pos[0] - pos[2], pos[1] + pos[3] ), "image_blitrect" : ( 0, 0, 22, 22 ) } )

        elif ( nearCity == "" ):
            self.hud.RemoveElement( "enterTown" )
            self.hud.RemoveElement( "enterTownIcon" )
            self.menu.Clear()
        


    def HandleEvents( self ):
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            if ( event.type == KEYDOWN ):
                if ( event.key == K_ESCAPE ):
                    self.gotoState = "titlestate"

            if event.type == MOUSEBUTTONUP:
                self.HandleMouse( event )

            if event.type == MOUSEMOTION:
                self.mouseX, self.mouseY = event.pos


    def Draw( self, windowSurface ):
        # windowSurface.blit( self.images["background"], ( 0, 0 ) )
        self.levelMap.DrawBelow( windowSurface, self.camera )
        self.DrawCities( windowSurface, self.levelMap.GetObjects(), self.camera )
        CharacterManager.Draw( windowSurface, self.camera )
        self.levelMap.DrawAbove( windowSurface, self.camera )
        self.hud.Draw( windowSurface )
        self.menu.Draw( windowSurface )
        self.camera.DebugDraw( windowSurface )
        EffectManager.Draw( windowSurface )

        windowSurface.blit( self.images["crosshairs"], ( self.mouseX - 24, self.mouseY - 24 ) )
        

    # TODO: Make city object
    def DrawCities( self, window, cities, camera=None ):
        for label in self.cityLabels:
            rect = pygame.Rect( label.fgTextPosition[0], label.fgTextPosition[1], 30, 30 )
            if ( ( camera is None ) or ( camera is not None and camera.DebugIsRegionVisible( rect ) ) ):
                posRect = rect
                if ( camera is not None ):
                    posRect = camera.GetAdjustedRect( posRect )

                window.blit( label.fgTextSurface, posRect )
                # TOOD: Don't draw label if not on screen

    def Clear( self ):
        self.levelMap.Clear()
        print( "Clear" )

    def LoadImages( self ):
        self.images["background"] = pygame.image.load( "assets/graphics/ui/background_title.png" )
        self.images["hudbg"] = pygame.image.load( "assets/graphics/hud-bg.png")
        self.images["phone_icons"] = pygame.image.load( "assets/graphics/phone-icons.png")
        self.images["items"] = pygame.image.load( "assets/graphics/items.png")
        self.images["player"] = pygame.image.load( "assets/graphics/character.png" )
        self.images["zombie"] = pygame.image.load( "assets/graphics/zombie.png" )
        self.images["handgun"] = pygame.image.load( "assets/graphics/weapons.png" )
        self.images["ammo"] = pygame.image.load( "assets/graphics/ammo.png" )
        self.images["bullet"] = pygame.image.load( "assets/graphics/bullet.png" )
        self.images["cheque"] = pygame.image.load( "assets/graphics/cheque.png" )
        self.images["fastfood"] = pygame.image.load( "assets/graphics/fastfood.png" )
        self.images["statement"] = pygame.image.load( "assets/graphics/letter.png" )
        self.images["keyboard"] = pygame.image.load( "assets/graphics/keyboard.png" )
        self.images["mouse"] = pygame.image.load( "assets/graphics/mouse.png" )
        self.images["tileset"] = pygame.image.load( "assets/graphics/tileset.png" )
        self.images["crosshairs"] = pygame.image.load( "assets/graphics/crosshairs.png" )
        self.images["town"] = pygame.image.load( "assets/graphics/town_kansascity.png" )
        self.images["heart"] = pygame.image.load( "assets/graphics/heart.png" )
        self.images["mouseicon"] = pygame.image.load( "assets/graphics/mouseicon.png" )
        self.images["buttonbg_ingame"] = pygame.image.load( "assets/graphics/ui/buttonbg_ingame.png" )
# hi



