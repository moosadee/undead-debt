#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.inventory import InventoryItem

class Job( InventoryItem ):
    def __init__( self, options=None ):
        super( Job, self ).__init__()
        self.pay = 0
        self.source = None
        self.destination = None
        self.type = ""
        
        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        super( Job, self ).Setup( options )
        self.pay = options["pay"]
        self.source = options["source"]
        self.destination = options["destination"]
        self.type = options["type"]
    
    def GetPay( self ):
        return self.pay
        
    def GetDestination( self ):
        return self.destination
        
    def GetType( self ):
        return self.type
        
    #def StartFoodDeliveryJob( self, destination, pay ):
        #Logger.Write( "Destination: " + destination, "SadPlayer::StartDeliveryJob", "#fcb3b3" )
        #self.destination = destination
        #self.job = "deliverfood"
        #self.inventory['job-item'] = "fastfood"
        #self.inventory['job-item'] = "statement"
        #self.jobPay = pay
    
